import os

import launch
import launch_ros.actions
from launch.actions import ExecuteProcess
from launch.substitutions import ThisLaunchFileDir,LaunchConfiguration
from launch.substitutions import ThisLaunchFileDir,LaunchConfiguration
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource


from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
     use_sim_time = LaunchConfiguration('use_sim_time', default=True)
     world_file_name = 'maritime.world'
     pkg_dir = '/home/saad/ros_ws/src/extended_kalman_filter'
     world = os.path.join(pkg_dir, 'world', world_file_name)
     launch_file_dir = os.path.join(pkg_dir, 'launch')
     gazebo = ExecuteProcess(
        cmd=['gazebo', '--verbose', world, '-s', 'libgazebo_ros_init.so',
              '-s', 'libgazebo_ros_factory.so'],
              output='screen')
     remappings = [("/imu/out","/imu/data")]
    
    
    
     ekf_param_dir = launch.substitutions.LaunchConfiguration(
        'ekf_param_dir',
        default=os.path.join(
            get_package_share_directory('kalman_filter_localization'),
            'param',
            'ekf.yaml'))

     ekf = launch_ros.actions.Node(
        package='kalman_filter_localization',
        node_executable='ekf_localization_node',
        parameters=[ekf_param_dir],
        remappings=[('/ekf_localization/gnss_pose', '/gnss_pose'),
                    ('/ekf_localization/imu', '/imu')],
        output='screen'
        )
     ekf__ = launch_ros.actions.Node(
        package='kalman_filter_localization',
        node_executable='ekf_initial_pose',
        output='screen'
        )


  #  tf = launch_ros.actions.Node(
  #      package='tf2_ros',
  #      node_executable='static_transform_publisher',
  #      arguments=['0', '0', '0', '0', '0', '0', '1', 'world', 'hull']
 #   )

     return launch.LaunchDescription([
        launch.actions.DeclareLaunchArgument(
            'ekf_param_dir',
            default_value=ekf_param_dir,
            description='Full path to ekf parameter file to load'),
        ekf,
        ekf__,
        gazebo,

   #     tf,

            ])
