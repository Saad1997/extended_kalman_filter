#ifndef KALMAN_FILTER_LOCALIZATION__EKF_LOCALIZATION_COMPONENT_HPP_
#define KALMAN_FILTER_LOCALIZATION__EKF_LOCALIZATION_COMPONENT_HPP_
#endif



#include <kalman_filter_localization/ekf.hpp>

#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geometry_msgs/msg/transform.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <geometry_msgs/msg/vector3_stamped.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <tf2/transform_datatypes.h>
#include <tf2/convert.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
//#include <interfaces/msg/velocity.hpp>
#include <kalman_filter_localization/msg/velocity.hpp>
#include <rclcpp_components/register_node_macro.hpp>

#include <Eigen/Core>

#include <string>

namespace kalman_filter_localization
{
class EkfLocalizationComponent : public rclcpp::Node
{
public:
 // KFL_EKFL_PUBLIC
  explicit EkfLocalizationComponent(const rclcpp::NodeOptions & options);
  rcl_interfaces::msg::SetParametersResult parametersCallback(
        const std::vector<rclcpp::Parameter> &parameters);

private:
  std::string reference_frame_id_;
  std::string robot_frame_id_;
  std::string initial_pose_topic_;
  std::string imu_topic_;
  std::string odom_topic_;
  std::string gnss_pose_topic_;
  int pub_period_;

  double var_imu_w_;// imu variance w value for quaternions may be
  double var_imu_acc_;//variance imu acceleration values
  double var_gnss_xy_;// variance value from gps sensor, may be position
  double var_gnss_z_;//variance z axis value from gps sensor
  Eigen::Vector3d var_gnss_;// may be a vector contating variance values of gps sensor for all axis
  double var_odom_xyz_;
  Eigen::Vector3d var_odom_;//contating variance odom information for all axis, .i.e. x y z
  bool use_gnss_;//may be wanna use gps sensor or not
  bool use_odom_;//may be wanna use odom sensor or not

  bool initial_pose_recieved_{false};

  geometry_msgs::msg::PoseStamped current_pose_;// ROS msg definition of type PoseStamped, provides frame_id, position of point and orientation of point in quartenions

  geometry_msgs::msg::PoseStamped error_pose_;
  kalman_filter_localization::msg::Velocity vel;//velocity from filter

  rclcpp::Time current_stamp_;// instance of Time class from rclcpp

  EKFEstimator ekf_;// instance for exteneded kalamn filter class in ekf.hpp

  rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr sub_initial_pose_;
  rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr sub_imu_;
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr sub_odom_;
  rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr sub_gnss_pose_;
  rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr current_pose_pub_;
  rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr error_pose_pub_;
  rclcpp::Publisher<kalman_filter_localization::msg::Velocity>::SharedPtr linear_vel_pub_;
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Clock clock_;
  tf2_ros::Buffer tfbuffer_;
  tf2_ros::TransformListener listener_;



  /**
   * @brief  used to provide predictionUpdate() from ekf with gyro and linear acceleration fro imu and current_time_imu.
   * @param imu_msg: message containing imu data
  **/
  void predictUpdate(const sensor_msgs::msg::Imu imu_msg);
  /**
   *@brief provide ekf.observationUpdate with y and variance
   *@param y position from pose msg
   * @param variance variance matrix from pose msg
   */
//  void measurementUpdate(const geometry_msgs::msg::PoseStamped pose_msg, const geometry_msgs::msg::Twist velocity, const Eigen::Vector3d variance);
   void measurementUpdate(const geometry_msgs::msg::PoseStamped pose_msg, const Eigen::Vector3d variance);
    /**
   *@brief publish current pose
   */
  void broadcastPose();

  geometry_msgs::msg::PoseStamped current_pose_odom_;
  Eigen::Matrix4d previous_odom_mat_{Eigen::Matrix4d::Identity()};


  enum STATE
  {
    X  = 0, Y = 1, Z = 2,
    VX = 3, VY = 4, VZ = 5,
    QX = 6, QY = 7, QZ = 8, QW = 9,
  };
};
}  // namespace kalman_filter_localization

//#endif  // KALMAN_FILTER_LOCALIZATION__EKF_LOCALIZATION_COMPONENT_HPP_
