#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geometry_msgs/msg/transform.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include "std_msgs/msg/string.hpp"
#include <tf2/transform_datatypes.h>
#include <tf2/convert.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
//#include <interfaces/msg/velocity.hpp>
#include <kalman_filter_localization/msg/velocity.hpp>
#include <rclcpp_components/register_node_macro.hpp>
#include <Eigen/Core>

using namespace std::chrono_literals;

class Initial_PosePublisher : public rclcpp::Node
{
public:

int i=0;

Initial_PosePublisher(): Node("initial_pose_publisher"),clock_(RCL_ROS_TIME),//RCL_ROS_TIME return system time
  tfbuffer_(std::make_shared<rclcpp::Clock>(clock_)),//std::make_shared creates object of type Clock and passes arguments to its constructor and return an object that stores and own pointer to this constructed object
  listener_(tfbuffer_)
    {

      declare_parameter("initial_pose_topic",std::string("/odom"));
      get_parameter("initial_pose_topic", initial_pose_topic_);
      declare_parameter("imu_topic",std::string("/imu/out"));
      get_parameter("imu_topic", imu_topic_);


     auto odom_callback =[this](const typename nav_msgs::msg::Odometry::SharedPtr msg) -> void
    {

    initial_pose_.header.stamp=msg->header.stamp;
    initial_pose_.header.frame_id=msg->child_frame_id;
    //initial_pose_.stamp=current_stamp_;
    initial_pose_.pose.position.x = 0;//msg->pose.pose.position.x;
    initial_pose_.pose.position.y = 0;//msg->pose.pose.position.y;
    initial_pose_.pose.position.z = 0;//msg->pose.pose.position.z;
    initial_pose_.pose.orientation.x = msg->pose.pose.orientation.x;
    initial_pose_.pose.orientation.y =msg->pose.pose.orientation.y;
    initial_pose_.pose.orientation.z = msg->pose.pose.orientation.z;
    initial_pose_.pose.orientation.w = msg->pose.pose.orientation.w;

    odom_pose_.header.stamp=msg->header.stamp;
    odom_pose_.header.frame_id=msg->child_frame_id;
    odom_pose_.pose.position.x=msg->pose.pose.position.x;
    odom_pose_.pose.position.y=msg->pose.pose.position.y;
    odom_pose_.pose.position.z=msg->pose.pose.position.z;
    odom_pose_.pose.orientation.x=msg->pose.pose.orientation.x;
    odom_pose_.pose.orientation.y=msg->pose.pose.orientation.y;
    odom_pose_.pose.orientation.z=msg->pose.pose.orientation.z;
    odom_pose_.pose.orientation.w=msg->pose.pose.orientation.w;

    velocity_from_sensor.header.stamp=msg->header.stamp;
    velocity_from_sensor.header.frame_id=msg->child_frame_id;
    velocity_from_sensor.x= msg->twist.twist.linear.x;
    velocity_from_sensor.y= msg->twist.twist.linear.y;
    velocity_from_sensor.z= -(msg->twist.twist.linear.z);

    };


    auto imu_callback =
    [this](const typename sensor_msgs::msg::Imu::SharedPtr msg) -> void
    {


          Imu_msg.header.stamp = msg->header.stamp;
          Imu_msg.angular_velocity.x = msg->linear_acceleration.x;
          Imu_msg.angular_velocity.y = msg->linear_acceleration.y;
          Imu_msg.angular_velocity.z = msg->linear_acceleration.z;
          Imu_msg.linear_acceleration.x = msg->angular_velocity.x;
          Imu_msg.linear_acceleration.y = msg->angular_velocity.y;
          Imu_msg.linear_acceleration.z = msg->angular_velocity.z;


          geometry_msgs::msg::Vector3Stamped acc_in, acc_out, w_in, w_out;
          acc_in.vector.x = msg->linear_acceleration.x;
          acc_in.vector.y = msg->linear_acceleration.y;
          acc_in.vector.z = msg->linear_acceleration.z;
          w_in.vector.x = msg->angular_velocity.x;
          w_in.vector.y = msg->angular_velocity.y;
          w_in.vector.z = msg->angular_velocity.z;
          transformed_msg.header.stamp = msg->header.stamp;

        //   std::cout<<transformed_msg.linear_acceleration.z <<std::endl;
       tf2::TimePoint time_point = tf2::TimePoint(
            std::chrono::seconds(msg->header.stamp.sec) +
            std::chrono::nanoseconds(msg->header.stamp.nanosec));
          const geometry_msgs::msg::TransformStamped transform =
            tfbuffer_.lookupTransform(
            "hull",
            msg->header.frame_id,
            time_point);
          tf2::doTransform(acc_in, acc_out, transform);
          tf2::doTransform(w_in, w_out, transform);
          transformed_msg.header.stamp = msg->header.stamp;
          transformed_msg.angular_velocity.x = w_out.vector.x;
          transformed_msg.angular_velocity.y = w_out.vector.y;
          transformed_msg.angular_velocity.z = w_out.vector.z;
          transformed_msg.linear_acceleration.x = acc_out.vector.x;
          transformed_msg.linear_acceleration.y = acc_out.vector.y;
          transformed_msg.linear_acceleration.z = acc_out.vector.z;

        };



    publisher_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("/ekf_localization/initial_pose", 10);
    odom_publisher_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("/ekf_localization/odom_pose", 10);
    vel_publisher_ = this->create_publisher<kalman_filter_localization::msg::Velocity>("/velocity_from_sensor", 10);
    imu_tf_publisher_=this->create_publisher<sensor_msgs::msg::Imu>("/imu/tf_data", 10);
    imu_publisher_=this->create_publisher<sensor_msgs::msg::Imu>("/imu/data", 10);


    subscriber_=  create_subscription<nav_msgs::msg::Odometry>(initial_pose_topic_, 10,odom_callback);
    imu_subscriber_=  create_subscription<sensor_msgs::msg::Imu>(imu_topic_, 10,imu_callback);

    std::chrono::milliseconds period(10);
    timer_ = create_wall_timer(
     std::chrono::duration_cast<std::chrono::nanoseconds>(period),std::bind(&Initial_PosePublisher::timer_callback, this));
    // period,std::bind(&Initial_PosePublisher::timer_callback, this));
    }

  private:
    std::string initial_pose_topic_;
    std::string imu_topic_;
    rclcpp::Clock clock_;
    tf2_ros::Buffer tfbuffer_;
    tf2_ros::TransformListener listener_;

    geometry_msgs::msg::PoseStamped initial_pose_;
    geometry_msgs::msg::PoseStamped odom_pose_;
    kalman_filter_localization::msg::Velocity velocity_from_sensor;
    sensor_msgs::msg::Imu transformed_msg;
    sensor_msgs::msg::Imu Imu_msg;

    rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr publisher_;
    rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr odom_publisher_;
    rclcpp::Publisher<kalman_filter_localization::msg::Velocity>::SharedPtr vel_publisher_;
    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr imu_tf_publisher_;
    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr imu_publisher_;

    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subscriber_;
    rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr imu_subscriber_;
    rclcpp::TimerBase::SharedPtr timer_;
    //rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;

     void timer_callback()
    {
      odom_publisher_->publish(odom_pose_);
      imu_tf_publisher_->publish(transformed_msg);
      imu_publisher_->publish(Imu_msg);
      vel_publisher_->publish(velocity_from_sensor);


      if(i<=1){
      publisher_->publish(initial_pose_);

     // vel_publisher_->publish(velocity_from_sensor);
      i++;
    }
    }

};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Initial_PosePublisher>());
  rclcpp::shutdown();
  return 0;
}
