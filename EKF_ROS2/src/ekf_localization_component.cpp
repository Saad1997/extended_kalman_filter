

#include <kalman_filter_localization/ekf_localization_component.hpp>
#include <chrono>
#include <vector>
#include <memory>
#include <string>
//#include <interfaces/msg/velocity.hpp>
#include <kalman_filter_localization/msg/velocity.hpp>
using namespace std::chrono_literals;

namespace kalman_filter_localization
{


EkfLocalizationComponent::EkfLocalizationComponent(const rclcpp::NodeOptions & options)
: Node("ekf_localization", options),// creating ROS node
  clock_(RCL_ROS_TIME),//RCL_ROS_TIME return system time
  tfbuffer_(std::make_shared<rclcpp::Clock>(clock_)),//std::make_shared creates object of type Clock and passes arguments to its constructor and return an object that stores and own pointer to this constructed object
  listener_(tfbuffer_)
{


  declare_parameter("reference_frame_id", "hull");//declare_parameter(const std::string name, const ParametreT & default value) declare and initiallize parametre
  get_parameter("reference_frame_id", reference_frame_id_);//get value of above initialiized parametre from node and store parametre value in reference_frame_id, i.e. 2nd parametre

  declare_parameter("initial_pose_topic", get_name() +std::string("/initial_pose"));
  get_parameter("initial_pose_topic", initial_pose_topic_);
  declare_parameter("imu_topic",std::string("/imu/out"));
  get_parameter("imu_topic", imu_topic_);
  declare_parameter("odom_topic",  std::string("/odom"));
  get_parameter("odom_topic", odom_topic_);
  declare_parameter("gnss_pose_topic", get_name() + std::string("/gnss_pose"));
  get_parameter("gnss_pose_topic", gnss_pose_topic_);

  declare_parameter("pub_period", 10);
  get_parameter("pub_period", pub_period_);
  declare_parameter("var_imu_w", 0.01);
  get_parameter("var_imu_w", var_imu_w_);
  declare_parameter("var_imu_acc", 0.01);
  get_parameter("var_imu_acc", var_imu_acc_);
  declare_parameter("var_gnss_xy", 0.1);
  get_parameter("var_gnss_xy", var_gnss_xy_);
  declare_parameter("var_gnss_z", 0.15);
  get_parameter("var_gnss_z", var_gnss_z_);
  declare_parameter("var_odom_xyz", 0.2);
  get_parameter("var_odom_xyz", var_odom_xyz_);
  declare_parameter("use_gnss", false);
  get_parameter("use_gnss", use_gnss_);
  declare_parameter("use_odom", true);
  get_parameter("use_odom", use_odom_);

  auto callback=
    [this](const std::vector<rclcpp::Parameter> params)
    -> rcl_interfaces::msg::SetParametersResult
    {
      auto results = std::make_shared<rcl_interfaces::msg::SetParametersResult>();
      for (auto param : params) {
        if (param.get_name() == "var_imu_w") {
          if (var_imu_w_ > 0) {
            var_imu_w_ = param.as_double();
            results->successful = true;
            results->reason = "";
          } else {
            results->successful = false;
            results->reason = "var_imu_w must over 0";
          }
        }
        if (param.get_name() == "var_imu_acc") {
          if (var_imu_acc_ > 0) {
            var_imu_acc_ = param.as_double();
            results->successful = true;
            results->reason = "";
          } else {
            results->successful = false;
            results->reason = "var_imu_acc must over 0";
          }
        }
        if (param.get_name() == "var_gnss_xy") {
          if (var_gnss_xy_ > 0) {
            var_gnss_xy_ = param.as_double();
            results->successful = true;
            results->reason = "";
          } else {
            results->successful = false;
            results->reason = "var_gnss_xy must over 0";
          }
        }
        if (param.get_name() == "var_gnss_z") {
          if (var_gnss_z_ > 0) {
            var_gnss_z_ = param.as_double();
            results->successful = true;
            results->reason = "";
          } else {
            results->successful = false;
            results->reason = "var_gnss_z must over 0";
          }
        }
      }
      if (!results->successful) {
        results->successful = false;
        results->reason = "";
      }
      return *results;
    };



  ekf_.setVarImuGyro(var_imu_w_);
  ekf_.setVarImuAcc(var_imu_acc_);
  var_gnss_ << var_gnss_xy_, var_gnss_xy_, var_gnss_z_;
  var_odom_ << var_odom_xyz_, var_odom_xyz_, var_odom_xyz_;


  // Setup Publisher
  std::string output_pose_name = get_name() + std::string("/current_pose");
  current_pose_pub_ = create_publisher<geometry_msgs::msg::PoseStamped>(output_pose_name, 10);
  std::string error_pose_name = get_name() + std::string("/error_pose");
  error_pose_pub_ = create_publisher<geometry_msgs::msg::PoseStamped>(error_pose_name, 10);

  std::string output_velocity = get_name() + std::string("/linear_velocity");
  linear_vel_pub_ = create_publisher<kalman_filter_localization::msg::Velocity>(output_velocity, 10);


  // Setup Subscriber
  auto initial_pose_callback =
    [this](const typename geometry_msgs::msg::PoseStamped::SharedPtr msg) -> void
    // [this](const typename nav_msgs::msg::Odometry::SharedPtr msg) -> void
    {
      std::cout << "initial pose callback" << std::endl;
      /*geometry_msgs::msg::PoseStamped orig_pose;
      orig_pose.header = msg->header;
      orig_pose.pose.position.x = msg->pose.position.x;
      orig_pose.pose.position.y = msg->pose.position.y;
      orig_pose.pose.position.z = msg->pose.position.z;
      orig_pose.pose.orientation.x = msg->pose.orientation.x;
      orig_pose.pose.orientation.y = msg->pose.orientation.y;
      orig_pose.pose.orientation.z = msg->pose.orientation.z;
      orig_pose.pose.orientation.w = msg->pose.orientation.w;

*/
      initial_pose_recieved_ = true;
     current_pose_ = *msg;
    //  current_pose_=orig_pose;
      Eigen::VectorXd x = Eigen::VectorXd::Zero(ekf_.getNumState());
      x(STATE::X) = current_pose_.pose.position.x;
      x(STATE::Y) = current_pose_.pose.position.y;
      x(STATE::Z) = current_pose_.pose.position.z;
      x(STATE::QX) = current_pose_.pose.orientation.x;
      x(STATE::QY) = current_pose_.pose.orientation.y;
      x(STATE::QZ) = current_pose_.pose.orientation.z;
      x(STATE::QW) = current_pose_.pose.orientation.w;


      ekf_.setInitialX(x);
    };




auto imu_callback =
    [this](const typename sensor_msgs::msg::Imu::SharedPtr msg) -> void
    {
      if (initial_pose_recieved_) {
        sensor_msgs::msg::Imu transformed_msg;
        try {
          geometry_msgs::msg::Vector3Stamped acc_in, acc_out, w_in, w_out;
          acc_in.vector.x = msg->linear_acceleration.x;
          acc_in.vector.y = msg->linear_acceleration.y;
          acc_in.vector.z = msg->linear_acceleration.z;
          w_in.vector.x = msg->angular_velocity.x;
          w_in.vector.y = msg->angular_velocity.y;
          w_in.vector.z = msg->angular_velocity.z;

          transformed_msg.header.stamp = msg->header.stamp;
          transformed_msg.angular_velocity.x = w_in.vector.x;
          transformed_msg.angular_velocity.y = w_in.vector.y;
          transformed_msg.angular_velocity.z = w_in.vector.z;
          transformed_msg.linear_acceleration.x = acc_in.vector.x;
          transformed_msg.linear_acceleration.y = acc_in.vector.y;
          transformed_msg.linear_acceleration.z = acc_in.vector.z;

          predictUpdate(transformed_msg);

        } catch (tf2::TransformException & e) {
          RCLCPP_ERROR(this->get_logger(), "%s", e.what());
          return;
        }
      }
    };




auto odom_callback =
    [this](const typename nav_msgs::msg::Odometry::SharedPtr msg) -> void
    {
    initial_pose_recieved_=true;
      if (initial_pose_recieved_ && use_odom_) {

        vel.header.stamp = msg->header.stamp;
        vel.header.frame_id = msg->child_frame_id;
        geometry_msgs::msg::PoseStamped pose;

        pose.pose.position.x = msg->pose.pose.position.x;
        pose.pose.position.y = msg->pose.pose.position.y;
        pose.pose.position.z = msg->pose.pose.position.z;
        measurementUpdate(pose, var_odom_);

      }


    };



auto gnss_pose_callback =
    [this](const typename geometry_msgs::msg::PoseStamped::SharedPtr msg) -> void
    {
      if (initial_pose_recieved_ && use_gnss_) {
//        measurementUpdate(*msg, var_gnss_);
      }
    };

 sub_initial_pose_ =create_subscription<geometry_msgs::msg::PoseStamped>(initial_pose_topic_, 1,initial_pose_callback);

  sub_imu_ =
    create_subscription<sensor_msgs::msg::Imu>(imu_topic_, 1,
      imu_callback);

  sub_odom_ =
    create_subscription<nav_msgs::msg::Odometry>(odom_topic_, 1,
      odom_callback);

  sub_gnss_pose_ =
    create_subscription<geometry_msgs::msg::PoseStamped>(gnss_pose_topic_, 1,
      gnss_pose_callback);

  std::chrono::milliseconds period(5);

  timer_ = create_wall_timer(
    std::chrono::duration_cast<std::chrono::nanoseconds>(period),std::bind(&EkfLocalizationComponent::broadcastPose, this));
   //period,std::bind(&EkfLocalizationComponent::broadcastPose, this));

}

void EkfLocalizationComponent::predictUpdate(const sensor_msgs::msg::Imu imu_msg)
{
  current_stamp_ = imu_msg.header.stamp;

  double current_time_imu = imu_msg.header.stamp.sec +
    imu_msg.header.stamp.nanosec * 1e-9;
  Eigen::Vector3d gyro = Eigen::Vector3d(
    imu_msg.angular_velocity.x,
    imu_msg.angular_velocity.y,
    imu_msg.angular_velocity.z);
  Eigen::Vector3d linear_acceleration = Eigen::Vector3d(
    imu_msg.linear_acceleration.x,
    imu_msg.linear_acceleration.y,
    imu_msg.linear_acceleration.z);

  ekf_.predictionUpdate(current_time_imu, gyro, linear_acceleration);
}


void EkfLocalizationComponent::measurementUpdate(
  const geometry_msgs::msg::PoseStamped pose_msg,
  const Eigen::Vector3d variance)
{
  current_stamp_ = pose_msg.header.stamp;
  Eigen::VectorXd y = Eigen::Vector3d(pose_msg.pose.position.x,
      pose_msg.pose.position.y,
      pose_msg.pose.position.z);//,pose_msg.pose.orientation.x,pose_msg.pose.orientation.y,pose_msg.pose.orientation.z);

  ekf_.observationUpdate(y, variance);
}


void EkfLocalizationComponent::broadcastPose()
{
  if (initial_pose_recieved_) {
    auto x = ekf_.getX();
     auto dx=ekf_.getdx();
    current_pose_.header.stamp = current_stamp_;
    current_pose_.header.frame_id = reference_frame_id_;
    current_pose_.pose.position.x = x(STATE::X);
    current_pose_.pose.position.y = x(STATE::Y);
    current_pose_.pose.position.z = x(STATE::Z);
    current_pose_.pose.orientation.x = x(STATE::QX);
    current_pose_.pose.orientation.y = x(STATE::QY);
    current_pose_.pose.orientation.z = x(STATE::QZ);
    current_pose_.pose.orientation.w = x(STATE::QW);
    error_pose_.header.stamp = current_stamp_;
    error_pose_.header.frame_id = reference_frame_id_;
    error_pose_.pose.position.x = dx(STATE::X);
    error_pose_.pose.position.y = dx(STATE::Y);
    error_pose_.pose.position.z = dx(STATE::Z);
    error_pose_.pose.orientation.x = dx(STATE::QX);
    error_pose_.pose.orientation.y = dx(STATE::QY);
    error_pose_.pose.orientation.z = dx(STATE::QZ);
    error_pose_.pose.orientation.w = dx(STATE::QW);

    vel.x=x(STATE::VX);
    vel.y=x(STATE::VY);
    vel.z=x(STATE::VZ);
    current_pose_pub_->publish(current_pose_);
     error_pose_pub_->publish(error_pose_);
    linear_vel_pub_->publish(vel);
  }
}
}  // namespace kalman_filter_localization

RCLCPP_COMPONENTS_REGISTER_NODE(kalman_filter_localization::EkfLocalizationComponent)
